/**
 * Created by mkarpinski on 18.09.17.
 */
angular.module('oauthApp', ['ui.router'])
  .config(function($stateProvider) {

      var root = {
        url: '/',
        template: '<ui-view></ui-view>'
      };

      var login = {
        url: 'login',
        templateUrl: 'app/login.html',
        controller: function ($scope, $http, $state) {
          $scope.login = function () {
            $http.post("/authenticate", {username: $scope.username, password: $scope.password})
              .then(function (response) {
                $http.defaults.headers.common.Authorization = 'bearer ' + response.data.access_token;
                $state.go('root.users');
                console.log(response.data);
              })
              .catch(function(error) {
                console.log(error);
              })
          }
        }
      };

      var users = {
        url: 'users',
        templateUrl: 'app/users.html',
        controller: function ($scope, $http, $state) {
          $http.get("/users")
            .then(function (response) {
              $scope.user = response.data;
            })
            .catch(function (error) {
              console.log(error);
              $state.go("root.login");
            })
        }
      };

      $stateProvider.state('root', root);
      $stateProvider.state('root.login', login);
      $stateProvider.state('root.users', users);
    }
  );