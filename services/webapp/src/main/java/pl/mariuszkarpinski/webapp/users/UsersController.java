package pl.mariuszkarpinski.webapp.users;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Created by mkarpinski on 18.09.17.
 */
@RestController
public class UsersController {

  @GetMapping("users")
  public ResponseEntity getUser(@RequestHeader HttpHeaders requestHeaders) {
    RestTemplate restTemplate = new RestTemplate();
    HttpEntity entity = new HttpEntity(requestHeaders);
    ResponseEntity responseEntity = restTemplate.exchange("http://localhost:8100/user", HttpMethod.GET, entity, Map.class);
    return responseEntity;
  }

  @ExceptionHandler(HttpClientErrorException.class)
  public ResponseEntity handleClient(HttpClientErrorException ex) {
    return ResponseEntity.status(ex.getStatusCode()).body(ex.getResponseBodyAsString());
  }

  @ExceptionHandler(HttpServerErrorException.class)
  public ResponseEntity handleClient(HttpServerErrorException ex) {
    return ResponseEntity.status(ex.getStatusCode()).body(ex.getResponseBodyAsString());
  }

}
