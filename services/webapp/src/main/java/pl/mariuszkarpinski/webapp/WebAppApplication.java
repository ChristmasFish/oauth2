package pl.mariuszkarpinski.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by mkarpinski on 18.09.17.
 */
@SpringBootApplication
public class WebAppApplication {

  public static void main(String... args) {
    SpringApplication.run(WebAppApplication.class, args);
  }

}
