package pl.mariuszkarpinski.webapp.authentication;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mkarpinski on 18.09.17.
 */
@RestController
public class AuthenticationController {

  private RestTemplate restTemplate = new RestTemplate();

  @PostMapping("authenticate")
  public ResponseEntity autheticate(@RequestBody User user) {
    String url = "http://localhost:8300/oauth/token";

    MultiValueMap<String, String> body= new LinkedMultiValueMap<String, String>();
    body.add("grant_type", "password");
    body.add("username", user.getUsername());
    body.add("password", user.getPassword());

    String auth = "clientId:clientSecret";
    byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("UTF-8")) );
    String authHeader = "Basic " + new String( encodedAuth );

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    headers.set(HttpHeaders.AUTHORIZATION, authHeader);

    HttpEntity entity = new HttpEntity(body, headers);

    ResponseEntity response = restTemplate.exchange(url, HttpMethod.POST, entity, Map.class);
    return response;
  }

  @ExceptionHandler(HttpClientErrorException.class)
  public ResponseEntity handleClient(HttpClientErrorException ex) {
    return ResponseEntity.status(ex.getStatusCode()).body(ex.getResponseBodyAsString());
  }

  @ExceptionHandler(HttpServerErrorException.class)
  public ResponseEntity handleClient(HttpServerErrorException ex) {
    return ResponseEntity.status(ex.getStatusCode()).body(ex.getResponseBodyAsString());
  }

}
