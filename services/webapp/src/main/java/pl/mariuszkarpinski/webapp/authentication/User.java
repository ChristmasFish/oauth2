package pl.mariuszkarpinski.webapp.authentication;

import lombok.Data;

/**
 * Created by mkarpinski on 18.09.17.
 */
@Data
public class User {

  private final String username;
  private final String password;

}
