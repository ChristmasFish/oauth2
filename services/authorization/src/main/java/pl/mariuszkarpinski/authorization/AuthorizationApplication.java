package pl.mariuszkarpinski.authorization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by mkarpinski on 18.09.17.
 */
@SpringBootApplication
public class AuthorizationApplication {

  public static void main(String... args) {
    SpringApplication.run(AuthorizationApplication.class, args);
  }

}
